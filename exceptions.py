# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.model.exceptions import ValidationError


class ProductMissingTaxError(UserError):
    pass


class ImportSalesError(UserError):
    pass


class SaleDeleteError(UserError):
    pass


class SaleForceDraftError(UserError):
    pass


class SaleDeviceError(UserError):
    pass


class DraftStatementError(ValidationError):
    pass


class PartyMissingAccount(UserError):
    pass


class StatementValidationError(UserError):
    pass
