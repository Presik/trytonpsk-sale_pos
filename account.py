# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.pool import Pool


class SaleAccountMovesStart(ModelView):
    'Sale Account Moves Start'
    __name__ = 'sale_pos.sale_account_moves.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    party = fields.Many2One('party.party', 'Party')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class SaleAccountMoves(Wizard):
    'Sale Account Moves'
    __name__ = 'sale_pos.sale_account_moves'
    start = StateView(
        'sale_pos.sale_account_moves.start',
        'sale_pos.sale_account_moves_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('sale_pos.sale_account_moves_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'shop': self.start.shop.id,
            'party': self.start.party.id if self.start.party else None
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleAccountMovesReport(Report):
    __name__ = 'sale_pos.sale_account_moves_report'

    @classmethod
    def get_line(cls, line, date_, number, origin):
        return {
            'date': date_,
            'move': number,
            'party': line.party.name if line.party else '',
            'reference': line.reference,
            'description': line.description,
            'origin': origin,
            'debit': line.debit,
            'credit': line.credit,
        }

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Statement = pool.get('account.statement')
        Company = pool.get('company.company')
        Shop = pool.get('sale.shop')
        company = Company(data['company'])
        shop = Shop(data['shop'])

        dom_st = [
            ('sale_device.shop', '=', data['shop']),
            ('date', '>=', data['start_date']),
            ('date', '<=', data['end_date']),
        ]
        dom_invoice = [
            ('shop', '=', data['shop']),
            ('invoice_date', '>=', data['start_date']),
            ('invoice_date', '<=', data['end_date']),
            ('move', '!=', None),
            ('type', '=', 'out'),
        ]
        if data.get('party'):
            dom_invoice.append(('party', '=', data['party']))

        statements = Statement.search(dom_st)

        _records = []
        rec_extend = _records.extend
        if data.get('party'):
            for st in statements:
                for line in st.lines:
                    if line.move and line.party.id == data['party']:
                        rec_extend(line.move.lines)
        else:
            for st in statements:
                for line in st.lines:
                    if line.move:
                        rec_extend(line.move.lines)

        invoices = Invoice.search(dom_invoice)
        for inv in invoices:
            rec_extend(inv.move.lines)

        records = {}
        for line in _records:
            account_id = line.account.id
            mv = line.move
            date_ = mv.date
            number = mv.number
            origin = mv.origin.rec_name if mv.origin else ''
            new_line = cls.get_line(line, date_, number, origin)
            try:
                records[account_id]['lines'].append(new_line)
                records[account_id]['total_debit'].append(line.debit)
                records[account_id]['total_credit'].append(line.credit)
            except:
                records[account_id] = {
                    'code': line.account.code,
                    'name': line.account.name,
                    'lines': [new_line],
                    'total_debit': [line.debit],
                    'total_credit': [line.credit],
                }

        report_context['shop'] = shop.name
        report_context['company'] = company
        report_context['records'] = records.values()
        return report_context
