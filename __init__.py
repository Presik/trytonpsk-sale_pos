# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import sale
from . import shop
from . import statement
from . import product
from . import device
from . import user
from . import invoice
from . import account


def register():
    Pool.register(
        invoice.Invoice,
        product.Template,
        product.Configuration,
        product.ConfigurationCodeRequired,
        product.Category,
        device.SaleDevice,
        user.User,
        statement.Journal,
        sale.Sale,
        sale.SaleLine,
        statement.Statement,
        statement.StatementLine,
        product.Product,
        shop.SaleShop,
        shop.ShopDailySummaryStart,
        shop.ShopDailyCategoryStart,
        sale.SaleUpdateDateStart,
        sale.SaleDetailedStart,
        sale.SaleIncomeDailyStart,
        sale.SaleByKindStart,
        sale.PortfolioPaymentsStart,
        sale.SalesCostsStart,
        sale.SalesAuditStart,
        account.SaleAccountMovesStart,
        device.SaleDeviceStatementJournal,
        statement.OpenStatementStart,
        statement.OpenStatementDone,
        statement.CloseStatementStart,
        statement.CloseStatementDone,
        statement.BillMoney,
        statement.MoneyCount,
        statement.ExpensesDaily,
        statement.AdditionalIncome,
        module='sale_pos', type_='model')
    Pool.register(
        account.SaleAccountMovesReport,
        shop.ShopDailySummaryReport,
        shop.ShopDailyCategoryReport,
        sale.SaleDetailedReport,
        sale.SaleIncomeDailyReport,
        sale.SaleByKindReport,
        sale.PortfolioPaymentsReport,
        sale.SalesCostsReport,
        sale.SalesAuditReport,
        module='sale_pos', type_='report')
    Pool.register(
        account.SaleAccountMoves,
        sale.SaleDetailed,
        sale.SaleForceDraft,
        sale.DeleteSalesDraft,
        sale.SaleUpdateDate,
        sale.SalesCosts,
        shop.ShopDailySummary,
        shop.ShopDailyCategory,
        sale.SaleIncomeDaily,
        sale.SaleByKind,
        sale.PortfolioPayments,
        sale.WizardSalePayment,
        statement.OpenStatement,
        statement.CloseStatement,
        sale.SalesAudit,
        module='sale_pos', type_='wizard')
